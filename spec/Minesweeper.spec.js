
describe('Minesweeper', () => {
  it('is playing on start', () => {
    const quantityOfMines = 0
    const boardConfig = { width: 1, height: 1 }

    const game = Minesweeper.startWith(boardConfig, quantityOfMines)

    expect(game.status()).toBe('playing')
  })

  it('is playing on start (with mines)', () => {
    const quantityOfMines = 1
    const boardConfig = { width: 1, height: 1 }

    const game = Minesweeper.startWith(boardConfig, quantityOfMines)

    expect(game.status()).toBe('playing')
  })

  it('is playing until all positions are cleared (1x1)', () => {
    const noMines = 0
    const boardConfig = { width: 1, height: 2 }
    const game = Minesweeper.startWith(boardConfig, noMines)

    game.check({ x: 1, y: 1 })

    expect(game.status()).toBe('playing')
  })

  it('is playing until all positions are cleared (2x2)', () => {
    const noMines = 0
    const boardConfig = { width: 2, height: 2 }
    const game = Minesweeper.startWith(boardConfig, noMines)

    game.check({ x: 1, y: 1 })
    game.check({ x: 1, y: 2 })

    expect(game.status()).toBe('playing')
  })

  it('is won when all the positions are checked without activate any mine (1x1)', () => {
    const noMines = 0
    const boardConfig = { width: 1, height: 1 }
    const game = Minesweeper.startWith(boardConfig, noMines)

    game.check({ x: 1, y: 1})

    expect(game.status()).toBe('won')
  })

  it('is won when all the positions are checked without activate any mine (1x2)', () => {
    const noMines = 0
    const boardConfig = { width: 1, height: 2 }
    const game = Minesweeper.startWith(boardConfig, noMines)

    game.check({ x: 1, y: 1 })
    game.check({ x: 1, y: 2 })

    expect(game.status()).toBe('won')
  })

  it('is won when all the positions are cleared without activate any mine (1x2 with mines)', () => {
    const quantityOfMines = 1
    const boardConfig = { width: 1, height: 2 }
    const game = Minesweeper.startWith(boardConfig, quantityOfMines)

    game.check({ x: 1, y: 2 })

    expect(game.status()).toBe('won')
  })

  it('is lost when a mine is activated (1x1 with mines)', () => {
    const quantityOfMines = 1
    const boardConfig = { width: 1, height: 1 }
    const game = Minesweeper.startWith(boardConfig, quantityOfMines)

    game.check({ x: 1, y: 1})

    expect(game.status()).toBe('lost')
  })

  it('is lost when a mine is activated (1x2 with mines)', () => {
    const quantityOfMines = 2
    const boardConfig = { width: 1, height: 2 }
    const game = Minesweeper.startWith(boardConfig, quantityOfMines)

    game.check({ x: 1, y: 2})

    expect(game.status()).toBe('lost')
  })
})
