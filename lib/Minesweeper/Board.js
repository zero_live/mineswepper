
const Board = class {
  constructor(config, quantityOfMines) {
    this.config = config

    const minesPositions = this._generateRandomPositionFor(quantityOfMines)
    this.mines = new Mines(minesPositions)
  }

  quantityOfMines() {
    return this.mines.quantity()
  }

  totalPositions() {
    return this.config.width * this.config.height
  }

  areThereMinesIn(position) {
    return this.mines.areThereIn(position)
  }

  areThereMines() {
    return !this.mines.isEmpty()
  }

  _generateRandomPositionFor(quantity) {
    const positions = []

    for(let currentPosition = 1; currentPosition <= quantity; currentPosition ++) {
      const position = Position.in({ x: 1, y: currentPosition })

      positions.push(position)
    }

    return positions
  }
}
