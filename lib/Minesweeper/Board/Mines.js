
const Mines = class {
  constructor(positions) {
    this.mines = positions
  }

  quantity() {
    return this.mines.length
  }

  isEmpty() {
    return (this.quantity > 0)
  }

  areThereIn(position) {
    return this.mines.some((minePosition => position.isIn(minePosition)))
  }
}
