
const Position = class {
  static in(coordinates) {
    return new Position(coordinates)
  }

  constructor(coordinates) {
    this.x = coordinates.x
    this.y = coordinates.y
  }

  isIn(otherPosition) {
    return (otherPosition.x === this.x && otherPosition.y === this.y)
  }
}
