
const Minesweeper = class {
  static startWith(boardConfig, quantityOfMines) {
    return new Minesweeper(boardConfig, quantityOfMines)
  }

  constructor(boardConfig, quantityOfMines) {
    this.board = new Board(boardConfig, quantityOfMines)

    this.isMineActivated = false
    this.quantityOfCheckedPositions = 0
  }

  check(coordinates) {
    const checkedPosition = Position.in(coordinates)
    this.quantityOfCheckedPositions += 1

    if (this._isThereMineIn(checkedPosition)) {
      this.isMineActivated = true
    }
  }

  status() {
    let status = 'playing'

    if (this._isBoardCleared()) {
      status = 'won'
    }

    if (this.isMineActivated) {
      status = 'lost'
    }

    return status
  }

  _quantityOfMines() {
    return this.board.quantityOfMines()
  }

  _isThereMineIn(position) {
    if(!this.board.areThereMines()) { return false }

    return this.board.areThereMinesIn(position)
  }

  _isBoardCleared() {
    if (!this._anyPositionChecked()) { return false }

    return this._areAllPositionChecked()
  }

  _anyPositionChecked() {
    return (this.quantityOfCheckedPositions > 0)
  }

  _areAllPositionChecked() {
    return (this.board.totalPositions() === (this.quantityOfCheckedPositions + this._quantityOfMines()))
  }
}
